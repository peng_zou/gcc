From e7013d2640d82e928ebdaf830b6833051ac65296 Mon Sep 17 00:00:00 2001
From: zhongyunde <zhongyunde@huawei.com>
Date: Sat, 5 Nov 2022 13:22:33 +0800
Subject: [PATCH 06/22] [MULL64 1/3] Add A ? B op CST : B match and simplify
 optimizations

    Refer to commit b6bdd7a4, use pattern match to simple
    A ? B op CST : B (where CST is power of 2) simplifications.
    Fixes the 1st issue of https://gitee.com/openeuler/gcc/issues/I5TSG0?from=project-issue.

    gcc/
            * match.pd (A ? B op CST : B): Add simplifcations for A ? B op POW2 : B

    gcc/testsuite/
            * gcc.dg/pr107190.c: New test.
---
 gcc/match.pd                    | 21 +++++++++++++++++++++
 gcc/testsuite/gcc.dg/pr107190.c | 27 +++++++++++++++++++++++++++
 2 files changed, 48 insertions(+)
 create mode 100644 gcc/testsuite/gcc.dg/pr107190.c

diff --git a/gcc/match.pd b/gcc/match.pd
index fc2833bbd..fd0857fc9 100644
--- a/gcc/match.pd
+++ b/gcc/match.pd
@@ -4280,6 +4280,27 @@ DEFINE_INT_AND_FLOAT_ROUND_FN (RINT)
 )
 #endif
 
+#if GIMPLE
+(if (canonicalize_math_p ())
+/* These patterns are mostly used by PHIOPT to move some operations outside of
+   the if statements. They should be done late because it gives jump threading
+   and few other passes to reduce what is going on.  */
+/* a ? x op C : x -> x op (a << log2(C)) when C is power of 2. */
+ (for op (plus minus bit_ior bit_xor lshift rshift lrotate rrotate)
+  (simplify
+   (cond @0 (op:s @1 integer_pow2p@2) @1)
+    /* powerof2cst */
+   (if (INTEGRAL_TYPE_P (type))
+    (with {
+      tree shift = build_int_cst (integer_type_node, tree_log2 (@2));
+     }
+     (op @1 (lshift (convert (convert:boolean_type_node @0)) { shift; })))
+   )
+  )
+ )
+)
+#endif
+
 /* Simplification moved from fold_cond_expr_with_comparison.  It may also
    be extended.  */
 /* This pattern implements two kinds simplification:
diff --git a/gcc/testsuite/gcc.dg/pr107190.c b/gcc/testsuite/gcc.dg/pr107190.c
new file mode 100644
index 000000000..235b2761a
--- /dev/null
+++ b/gcc/testsuite/gcc.dg/pr107190.c
@@ -0,0 +1,27 @@
+/* { dg-do compile } */
+/* { dg-options "-O2 -fexpensive-optimizations -fdump-tree-phiopt2-details" } */
+
+#  define BN_BITS4        32
+#  define BN_MASK2        (0xffffffffffffffffL)
+#  define BN_MASK2l       (0xffffffffL)
+#  define BN_MASK2h       (0xffffffff00000000L)
+#  define BN_MASK2h1      (0xffffffff80000000L)
+#  define LBITS(a)        ((a)&BN_MASK2l)
+#  define HBITS(a)        (((a)>>BN_BITS4)&BN_MASK2l)
+#  define L2HBITS(a)      (((a)<<BN_BITS4)&BN_MASK2)
+
+unsigned int test_m(unsigned long in0, unsigned long in1) {
+    unsigned long m, m1, lt, ht, bl, bh;
+    lt = LBITS(in0);
+    ht = HBITS(in0);
+    bl = LBITS(in1);
+    bh = HBITS(in1);
+    m  = bh * lt;
+    m1 = bl * ht;
+    ht = bh * ht;
+    m  = (m + m1) & BN_MASK2;
+    if (m < m1) ht += L2HBITS((unsigned long)1);
+    return ht + m;
+}
+
+/* { dg-final { scan-tree-dump "COND_EXPR in block 2 and PHI in block 4 converted to straightline code" "phiopt2" } } */
-- 
2.33.0

